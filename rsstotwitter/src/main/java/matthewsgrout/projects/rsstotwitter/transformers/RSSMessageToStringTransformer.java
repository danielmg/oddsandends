package matthewsgrout.projects.rsstotwitter.transformers;

import static com.rosaloves.bitlyj.Bitly.as;
import static com.rosaloves.bitlyj.Bitly.shorten;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.jdom.Element;
import org.jdom.Text;
import org.springframework.integration.Message;

import com.sun.syndication.feed.synd.SyndEntry;
public class RSSMessageToStringTransformer {
	
	private String bitlyApiKey;
	private String bitlyApiUser;
	//http://code.google.com/p/bitlyj/
	private static Logger logger = Logger.getLogger(RSSMessageToStringTransformer.class.getName());
	
    @SuppressWarnings("unchecked")
	public String transform(Message<SyndEntry> message) {  
    	
        SyndEntry se = message.getPayload();  
        
        //This code is specific to the Apple RSS layout
        
        //Title Element
        String title =((Text)((ArrayList<Element>)se.getForeignMarkup()).get(0).getContent().get(0)).getValue();
        //Artist Element
        String artist = ((Text)((ArrayList<Element>)se.getForeignMarkup()).get(2).getContent().get(0)).getValue();
        //Price Element
        String price = ((Text)((ArrayList<Element>)se.getForeignMarkup()).get(3).getContent().get(0)).getValue();
        //Release Date
        String releaseDate = ((Text)((ArrayList<Element>)se.getForeignMarkup()).get(7).getContent().get(0)).getValue();
        
        //get a bitly URL for the iTunes link
        String url=as(this.bitlyApiUser,this.bitlyApiKey).call(shorten(se.getUri())).getShortUrl();
     
        //Large image element - still need to figure out how to get this in
        //String img = ((Text)((ArrayList<Element>)se.getForeignMarkup()).get(6).getContent().get(0)).getValue();
      
        // String imgUrl = as(this.bitlyApiUser,this.bitlyApiKey).call(shorten(img)).getShortUrl();
        title='#'+StringUtils.remove(title, ' ');
        artist='#'+StringUtils.remove(artist, ' ');
        String msg = title +  " - " + artist + "\n Released: " + releaseDate.substring(0,10) + " (" + price +")\n" + url; 
        logger.info("tweeted: " + se.getTitle());
        //TRIM  
        return msg.length()>140?msg.substring(0, 140):msg;  
   }

	public void setBitlyApiKey(String bitlyApiKey) {
		this.bitlyApiKey = bitlyApiKey;
	}

	public void setBitlyApiUser(String bitlyApiUser) {
		this.bitlyApiUser = bitlyApiUser;
	}  
    
    
    
}
