========================
Configuration
========================

The configuration is located in src/main/resources/rsstotwitter.properties.

You will need to generate the following Twitter API keys (r/w):

twitter.oauth.consumerKey=
twitter.oauth.consumerSecret=

and your personal Twitter keys (r/w):

twitter.oauth.accessToken=
twitter.oauth.accessTokenSecret=

and your bitly API key and username (https://bitly.com/a/your_api_key	):

bitly.api.key=
bitly.api.user=

------------------------

The URL configuration is where to get the RSS feed from.  Please note that this program is coded to 
specifically get fields from the iTunes RSS format.

If you want to change the format then edit the class RSSMessageToStringTransformer.  

This can be found under src/main/java/matthewsgrout/projects/rsstotwitter/transformers/RSSMessageToStringTransformer.java

========================
Build
========================

I've set this up with a Maven build file, so it is really easier to produce a build once you 
have done the above configuration.

Maven can be obtained here:  http://maven.apache.org/

To execute the build simply fire up a command prompt, navigate to where the pom.xml is, e.g.
"cd c:\users\daniel\documents\workspace\rsstotwitter"

then "mvn clean package"

This will then create a .jar file in the directory called target.  You want the one caled rsstotwitter-1.0.jar.

========================
Run
========================

From the command line, in the directory where the jar file is: "java -jar rsstotwitter-1.0.jar"
