<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
</head>
<body>
		<p>Title: <s:property value="person.title"/></p>
		<p>First name: <s:property value="person.firstName"/></p>
		<p>Surname: <s:property value="person.surName"/><p>
		<p>Address: <s:property value="person.address"/></p>
		<p>Age in years: <s:property value="person.ageInYears"/></p>
		<p>Age in days: <s:property value="person.ageInDays"/></p>
</body>
</html>