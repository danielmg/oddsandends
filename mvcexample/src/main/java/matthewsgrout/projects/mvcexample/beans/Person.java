package matthewsgrout.projects.mvcexample.beans;

public class Person {

	private String firstName;
	private String title;
	private String surname;
	private String address;
	private int ageInYears;
	private int ageInDays;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getAgeInYears() {
		return ageInYears;
	}
	public void setAgeInYears(int ageInYears) {
		this.ageInYears = ageInYears;
	}
	public int getAgeInDays() {
		return ageInDays;
	}
	public void setAgeInDays(int ageInDays) {
		this.ageInDays = ageInDays;
	}
	
}
